#include <linux/init.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/uaccess.h> // for copy_to_user
#include <linux/slab.h> // for kmalloc

#define FIB_DEV_NAME "fibonacci"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ahmad Karimi");
MODULE_DESCRIPTION("Calculates fibonacci series.");
MODULE_VERSION("0.01");

static bool is_open = false;
static dev_t fib_dev = 1;
static struct cdev *fib_cdev;

// prototypes
static int fib_open(struct inode *, struct file *);
static int fib_release(struct inode *, struct file *);
static ssize_t fib_read(struct file *, char *, size_t, loff_t *);
static loff_t fib_llseek(struct file *, loff_t, int);

const struct file_operations fib_fops = {
    .owner = THIS_MODULE,
    .read = fib_read,
    .llseek = fib_llseek,
    .release = fib_release,
    .open = fib_open
};

static loff_t fib_llseek(struct file *file, loff_t offset, int whence) {
    file->f_pos = file->f_pos + offset;
    return file->f_pos;
}

static ssize_t fib_read(struct file *file_ptr,
                        char *user_buffer,
                        size_t buff_size,
                        loff_t *offset) {
    int n = *offset;

    int fib[n + 2];
    fib[0] = 0;
    fib[1] = 1;
    if (n == 0) {
        copy_to_user(user_buffer, fib, buff_size);
        return 1;
    }

    for (int i = 2; i < n + 2; ++i) {
        fib[i] = fib[i - 1] + fib[i - 2];
    }
    copy_to_user(user_buffer, fib, buff_size);
 
    return n;
}

static int fib_open(struct inode *inode, struct file *file) {
    if (is_open) {
        return -EBUSY;
    }

    is_open = true;
    return 0;
}

static int fib_release(struct inode *inode, struct file *file) {
    is_open = false;
    return 0;
}

static int __init fib_init(void) {
    int result = 0;
    result = alloc_chrdev_region(&fib_dev, 0, 1, FIB_DEV_NAME);
    if (result < 0) {
        printk(KERN_ALERT "alloc_chrdev_region failed");
        return result;
    }

    fib_cdev = cdev_alloc();
    if (fib_cdev == NULL) {
        printk(KERN_ALERT "cdev_alloc failed");
        return -1;
    }
    cdev_init(fib_cdev, &fib_fops);

    result = cdev_add(fib_cdev, fib_dev, 1);
    if (result < 0) {
        printk(KERN_ALERT "cdev_init or cdev_add failed");
        result = -2;
        goto failed_cdev;
    }

    return result;

failed_cdev:
    cdev_del(fib_cdev);
    unregister_chrdev_region(fib_dev, 1);

    return result;
}

static void __exit fib_exit(void) {
    cdev_del(fib_cdev);
    unregister_chrdev_region(fib_dev, 1);

    printk(KERN_INFO "fib_kernel module exited.");
}

module_init(fib_init);
module_exit(fib_exit);

