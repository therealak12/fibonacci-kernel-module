#include <fcntl.h> // for file modes like O_RDWR
#include <stdio.h>
#include <string.h>
#include <unistd.h> // for open, lseek, read, ...
#include <errno.h> // for error numbers

#define FIB_DEV_NAME "/dev/fibonacci"

int main() {
    int fd = open(FIB_DEV_NAME, O_RDWR);
    if (fd < 0) {
        perror("failed to open fibonacci character device");
        return 1;
    }

    long long which_term;
    printf("Which term of the fibonacci sequence you want? ");
    scanf("%lld", &which_term);
    which_term += 1;

    int read_buf[which_term];
    lseek(fd, which_term, SEEK_SET);
    long long r_result = read(fd, &read_buf, sizeof(read_buf));
    if (r_result < 0) {
        perror("Failed reading from device ");
        return errno;
    }

    printf("r_result is %lld\n", r_result);
    printf("The resulting fibonacci number: ");
    for (int i = 0; i < which_term; ++i) {
        printf("%d ", read_buf[i]);
    }

    close(fd);
    return 0;
}

